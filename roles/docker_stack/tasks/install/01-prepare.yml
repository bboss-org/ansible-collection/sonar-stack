---
# BBOSS Copyright (C) 2018-2021, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

#   Installation steps : (install)
#    -> 1. prepare
#       2. download
#       3. configure
#       4. activate

- name: Prepare - Install required dependencies
  ansible.builtin.package:
    state: present
    name: "{{ item }}"
  loop: >-
    {%- if ansible_os_family == "RedHat" %}
      {%- set package_list = ["epel-release", "python-virtualenv",
        "cloud-init", "cloud-utils-growpart"] %}
    {%- else %}
      {%- set package_list = ["virtualenv",
        "cloud-init"] %}
    {%- endif %}
    {{- package_list | union(["python3-pip"
      if (ansible_python.version.major == 3) else "python-pip"]) }}

- name: Prepare - Create project directory
  ansible.builtin.file:
    path: "{{ stack_project.path }}"
    state: directory

- name: Prepare - Create virtualenv and Update pip
  ansible.builtin.pip:
    state: present
    # pip 21.0 dropped support for Python 2.7 and Python 3.5 in January 2021
    name: "{{ 'pip' if (ansible_python.version.major == 3 and
      ansible_python.version.minor >= 6) else 'pip==20.2.*' }}"
    extra_args: -U
    virtualenv: "{{ stack_project.path }}venv/"
    virtualenv_site_packages: yes

- name: Prepare - Install python dependencies
  ansible.builtin.pip:
    state: present
    # pyrsistent is required by docker-compose, the version 0.17.2 does not
    # support python 2.7 and pip does not always choose the right one.
    name:
      - cryptography
      - "{{ 'pyrsistent' if ansible_python.version.major == 3 else
        'pyrsistent==0.16.1' }}"
      - docker
      - docker-compose
    virtualenv: "{{ stack_project.path }}venv/"

- name: Prepare - Create docker-compose.yml
  ansible.builtin.template:
    src: docker-compose.yml.j2
    dest: "{{ stack_project.path }}docker-compose.yml"
  notify: Restart systemd

- name: Prepare - Create systemd service configuration
  ansible.builtin.template:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}"
  loop:
    - src: docker-compose.service.j2
      dest: "/etc/systemd/system/{{ stack_project.name }}.service"
  notify: Reload systemd

- name: Prepare - Configure sysctl (SonarQube Specific)
  ansible.posix.sysctl:
    name: "{{ item.name }}"
    value: "{{ item.value }}"
    state: present
  loop:
    - {name: "vm.max_map_count", value: 524288}
    - {name: "fs.file-max", value: 131072}
