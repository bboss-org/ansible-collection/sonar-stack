---
# BBOSS Copyright (C) 2018-2021, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

- name: "{{ ssl_title_prefix }}Create configuration directories"
  ansible.builtin.file:
    path: "{{ item }}"
    state: directory
    owner: root
    group: root
  loop:
    - "{{ ssl_key_path }}"
    - "{{ ssl_crt_path }}"

# Use existing certificates content
- name: "{{ ssl_title_prefix }}Copy existing private keys"
  ansible.builtin.copy:
    content: "{{ _stackapp_certificates[item].key }}"
    dest: "{{ path }}"
    owner: root
    group: root
  vars:
    path: "{{ ssl_key_path + stackapp_network[item].fqdn }}.key"
  loop: "{{ _stackapp_certificates | dict2items | community.general.json_query(
    '[?value.key!=nil].key') }}"
  loop_control:
    label: "{{ path }}"

- name: "{{ ssl_title_prefix }}Copy existing certificates"
  ansible.builtin.copy:
    content: "{{ _stackapp_certificates[item].crt }}"
    dest: "{{ path }}"
    owner: root
    group: root
  vars:
    path: "{{ ssl_crt_path + stackapp_network[item].fqdn }}.crt"
  loop: "{{ _stackapp_certificates | dict2items | community.general.json_query(
    '[?(value.key!=nil) && (value.crt!=nil)].key') }}"
  loop_control:
    label: "{{ path }}"

# Generate missing certificates
- name: "{{ ssl_title_prefix }}Generate private keys"
  community.crypto.openssl_privatekey:
    path: "{{ path }}"
  vars:
    path: "{{ ssl_key_path + stackapp_network[item].fqdn }}.key"
  loop: "{{ _stackapp_certificates | dict2items | community.general.json_query(
    '[?value.key==nil].key') }}"
  loop_control:
    label: "{{ path }}"

- name: "{{ ssl_title_prefix }}Generate certificate requests"
  community.crypto.openssl_csr:
    path: "{{ path }}"
    privatekey_path: "{{ ssl_key_path + stackapp_network[item].fqdn }}.key"
    common_name: "{{ stackapp_network[item].fqdn }}"
    subject_alt_name: "{{ value_csr.subject_alt_name | default(omit) }}"
  vars:
    path: "{{ ssl_csr_path + stackapp_network[item].fqdn }}.csr"
    value_csr: "{{ _stackapp_certificates[item].csr |
      default({'subject_alt_name': []}) }}"
  loop: "{{ _stackapp_certificates | dict2items | community.general.json_query(
    '[?(value.key==nil) || (value.crt==nil)].key') }}"
  loop_control:
    label: "{{ path }}"
  register: _stackapp_task_generate_csr

- name: "{{ ssl_title_prefix }}Sign certificate requests"
  community.crypto.x509_certificate:
    path: "{{ path }}"
    csr_path: "{{ result.filename }}"
    provider: ownca
    ownca_content: "{{ ssl_ca_crt }}"
    ownca_privatekey_content: "{{ ssl_ca_key }}"
  vars:
    path: "{{ ssl_crt_path + stackapp_network[result.item].fqdn }}.crt"
  loop: "{{ _stackapp_task_generate_csr.results }}"
  loop_control:
    loop_var: result
    label: "{{ path }}"

# Cleanup temporary files
# - name: "{{ ssl_title_prefix }}Cleanup certificate requests"
#   ansible.builtin.file:
#     path: "{{ item }}"
#     state: absent
#   loop: "{{ _stackapp_task_generate_csr.results |
#       community.general.json_query('[*].filename') }}"
