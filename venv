#!/usr/bin/env bash

# BBOSS Copyright (C) 2018-2021, Laurent Marchelli
# SPDX-License-Identifier: GPL-3.0-or-later

# This file must be used with "source venv" *from current bash shell*.

# Create and Activate python environnement and configure ansible with the
# hypervisor inventory.
VENV_SCRIPT_PATH=$(readlink -f "${BASH_SOURCE[0]}")
VENV_SCRIPT_PATH=$(dirname "${VENV_SCRIPT_PATH}")/
if [[ -e "${VENV_SCRIPT_PATH}venv.env" ]]; then
  # shellcheck disable=SC1090
  source "${VENV_SCRIPT_PATH}venv.env" "${@}"
fi
VENV_SCRIPT_PROJ=${VENV_SCRIPT_PROJ:-$(basename "${VENV_SCRIPT_PATH}")}
VENV_SCRIPT_HOME=${VENV_SCRIPT_HOME:-${HOME}/.${VENV_SCRIPT_PROJ}/venv/}
# shellcheck disable=SC2086
export ${!VENV_SCRIPT*}

# Check exit conditions
if [[ "${0}" == "${BASH_SOURCE[0]}" ]]; then
  echo "[ERROR] This file must be sourced with 'source venv'"
else
  # Create and activate python virtual environment
  [[ -n "${VIRTUAL_ENV}" ]] && deactivate
  if [[ ! -e "${VENV_SCRIPT_HOME}" ]]; then
    python3 -m venv "${VENV_SCRIPT_HOME}" --prompt "${VENV_SCRIPT_PROJ}"
    # shellcheck disable=SC1090
    source "${VENV_SCRIPT_HOME}bin/activate"
    pip install --upgrade pip wheel setuptools
    if [[ -e "${VENV_SCRIPT_PATH}venvreq.txt" ]]; then
      pip install -r "${VENV_SCRIPT_PATH}venvreq.txt"
    else
      pip install ansible jmespath
      pip install ansible-lint==4.3.* yamllint molecule==3.2.* molecule-vagrant python-vagrant
      pip freeze > "${VENV_SCRIPT_PATH}venvreq.txt"
    fi
  else
    # shellcheck disable=SC1090
    source "${VENV_SCRIPT_HOME}bin/activate"
  fi
  # Configure hypervisor variables
  VENV_HYPER_BOX_NAME="${1}"
  if [[ -n "${VENV_HYPER_BOX_NAME}" ]]; then
    ANSIBLE_INVENTORY="${VENV_SCRIPT_PATH}.vagrant/provisioners/ansible/inventory/"
    ANSIBLE_HOST_KEY_CHECKING="False"
    PS1="${PS1/)/:${VENV_HYPER_BOX_NAME})}"
    export ANSIBLE_INVENTORY
    export ANSIBLE_HOST_KEY_CHECKING
    export PS1
  else
    unset ANSIBLE_INVENTORY
    unset ANSIBLE_HOST_KEY_CHECKING
  fi

  # Move to project directory
  # shellcheck disable=SC2164
  cd "${VENV_SCRIPT_PATH}"

  # Install galaxy requirements
  if [[ -f "${VENV_SCRIPT_PATH}requirements.yml" ]]; then
    ansible-galaxy collection install -r "${VENV_SCRIPT_PATH}requirements.yml"
    ansible-galaxy role install -r "${VENV_SCRIPT_PATH}requirements.yml"
  fi

  # Hypervisor box specifications based on friendly name
  if [[ "${VENV_HYPER_BOX_NAME}" == "centos7.6" ]]; then
    export VENV_HYPER_BOX_IMG="centos/7"
    export VENV_HYPER_BOX_VER="~> 1905"
  elif [[ "${VENV_HYPER_BOX_NAME}" == "centos7.8" ]]; then
    export VENV_HYPER_BOX_IMG="centos/7"
    export VENV_HYPER_BOX_VER="~> 2004"
  elif [[ "${VENV_HYPER_BOX_NAME}" == "stretch" ]]; then
    export VENV_HYPER_BOX_IMG="debian/stretch64"
    export VENV_HYPER_BOX_VER=">= 0"
  elif [[ "${VENV_HYPER_BOX_NAME}" == "buster" ]]; then
    export VENV_HYPER_BOX_IMG="debian/buster64"
    export VENV_HYPER_BOX_VER=">= 0"
  elif [[ "${VENV_HYPER_BOX_NAME}" == "bionic" ]]; then
    export VENV_HYPER_BOX_IMG="ubuntu/bionic64"
    export VENV_HYPER_BOX_VER=">= 0"
  elif [[ "${VENV_HYPER_BOX_NAME}" == "focal" ]]; then
    export VENV_HYPER_BOX_IMG="ubuntu/focal64"
    export VENV_HYPER_BOX_VER=">= 0"
  else
    unset VENV_HYPER_BOX_NAME
    unset VENV_HYPER_BOX_IMG
    unset VENV_HYPER_BOX_VER
  fi

  # Hypervisor network specifications
  if [[ -n "${VENV_HYPER_BOX_NAME}" ]]; then
    if [[ -z "${VENV_HYPER_NET_FQDN}" ]]; then
      if [[ -n "${BUILD_SCT_PRODUCT}" ]]; then
        VENV_HYPER_NET_FQDN="${BUILD_SCT_PRODUCT}"
      else
        VENV_HYPER_NET_FQDN=$(dirname "${VENV_SCRIPT_PATH}")
        VENV_HYPER_NET_FQDN=$(basename "${VENV_HYPER_NET_FQDN}")
      fi
      if [[ -n "${BUILD_SCT_NETDOMAIN}" ]]; then
        VENV_HYPER_NET_FQDN="${VENV_HYPER_NET_FQDN}.${BUILD_SCT_NETDOMAIN}"
      else
        VENV_HYPER_NET_FQDN="${VENV_HYPER_NET_FQDN}.$(hostname -d)"
      fi
    fi
    export VENV_HYPER_NET_FQDN
    export VENV_HYPER_NET_PORT="${VENV_HYPER_NET_PORT:-8443}"
  else
    unset VENV_HYPER_NET_FQDN
    unset VENV_HYPER_NET_PORT
  fi
fi
